

/*
 * Firmware for Zephyr ADC Lab
 *
 * Developed by Kyle Duerr
 */

#include <zephyr/kernel.h>
#include <zephyr/device.h>
#include <zephyr/devicetree.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/drivers/adc.h> 
#include <zephyr/smf.h>
#include <zephyr/logging/log.h>
#include <zephyr/drivers/pwm.h>
#include <nrfx_power.h> // NOTE: This is not a Zephyr library!!  It is a Nordic NRFX library.
#include "pressure.h"

#include <zephyr/bluetooth/bluetooth.h>
#include <zephyr/bluetooth/uuid.h>
#include <zephyr/bluetooth/gatt.h>
#include <zephyr/bluetooth/hci.h> // host controller interface
#include <zephyr/bluetooth/services/bas.h>  // battery service GATT
#include <zephyr/settings/settings.h>

LOG_MODULE_REGISTER(main,LOG_LEVEL_DBG);

/* DEFINE MACROS */
/* Define macros for UUIDs of the Remote Service
   Project ID: 001 (3rd entry)
   MFG ID = 0x01FF (4th entry)
*/
#define BT_UUID_REMOTE_SERV_VAL \
        BT_UUID_128_ENCODE(0xe9ea0000, 0xe19b, 0x0001, 0x01FF, 0xc7907585fc48)
#define BT_UUID_REMOTE_SERVICE 			BT_UUID_DECLARE_128(BT_UUID_REMOTE_SERV_VAL)

/* UUID of the Data Characteristic */
#define BT_UUID_REMOTE_DATA_CHRC_VAL \
        BT_UUID_128_ENCODE(0xe9ea0001, 0xe19b, 0x0001, 0x01FF, 0xc7907585fc48)
#define BT_UUID_REMOTE_DATA_CHRC 		BT_UUID_DECLARE_128(BT_UUID_REMOTE_DATA_CHRC_VAL)

#define DEVICE_NAME CONFIG_BT_DEVICE_NAME
#define DEVICE_NAME_LEN (sizeof(DEVICE_NAME)-1)
#define BLE_DATA_POINTS 600 // limited by MTU

#define SAVE_BUTTON DT_ALIAS(savebutton)
#define SEND_BUTTON DT_ALIAS(sendbutton)
#define HEARTBEAT_LED DT_ALIAS(heartbeat)
#define ERROR_LED DT_ALIAS(vbus)
#define LED_100 DT_ALIAS(led100hz)
#define LED_500 DT_ALIAS(led500hz)
#define LED_ON_TIME_MS 500
#define SLEEP_TIME_MS 10
#define SAVE_BTN_PRESS BIT(0)
#define SEND_BTN_PRESS BIT(1)
#define ADC_100_FULL BIT(2)
#define ADC_500_FULL BIT(3)
#define MAX_BATTERY_DV 36 // "decivolts", makes math from mV to percentage easier
#define ADC_READINGS_PER_PEAK 20
#define ADC_RECORD_TIME_MS 1000
#define FREQ_100_HZ 100
#define FREQ_500_HZ 500
#define MAX_ADC_READINGS_100 (ADC_READINGS_PER_PEAK*FREQ_100_HZ)
#define MAX_ADC_READINGS_500 (ADC_READINGS_PER_PEAK*FREQ_100_HZ)
#define RECORD_TIME_100_US ((ADC_RECORD_TIME_MS*1000)/MAX_ADC_READINGS_100)
#define RECORD_TIME_500_US ((ADC_RECORD_TIME_MS*1000)/MAX_ADC_READINGS_500)
#define POINTS_PER_SET_100_HZ ADC_READINGS_PER_PEAK
#define POINTS_PER_SET_500_HZ ADC_READINGS_PER_PEAK
#define MAX_LED_BRIGHTNESS 1000000
#define PWM_BRIGHTNESS_TO_PERCENT 10000
#define MEASUREMENT_DELAY_MS 1000  // delay between measurements
#define OVERSAMPLE 10  // number of samples to average together

/* Define some macros to use some Zephyr macros to help read the DT configuration */
#define ADC_DT_SPEC_GET_BY_ALIAS(adc_alias)                    \
{                                                            \
    .dev = DEVICE_DT_GET(DT_PARENT(DT_ALIAS(adc_alias))),      \
    .channel_id = DT_REG_ADDR(DT_ALIAS(adc_alias)),            \
    ADC_CHANNEL_CFG_FROM_DT_NODE(DT_ALIAS(adc_alias))          \
}                                                            \

#define DT_SPEC_AND_COMMA(node_id, prop, idx) \
    ADC_DT_SPEC_GET_BY_IDX(node_id, idx),

/* INITIALIZE GLOBAL VARIABLES */

/* Bluetooth */
// enumeration to keep track of the state of the notifications
enum bt_data_notifications_enabled {
    BT_DATA_NOTIFICATIONS_ENABLED,
    BT_DATA_NOTIFICATIONS_DISABLED,
};
enum bt_data_notifications_enabled notifications_enabled;

// declare struct of the BLE remove service callbacks
struct bt_remote_srv_cb {
    void (*notif_changed)(enum bt_data_notifications_enabled status);
    void (*data_rx)(struct bt_conn *conn, const uint8_t *const data, uint16_t len);
}; 
struct bt_remote_srv_cb remote_service_callbacks;

// blocking thread semaphore to wait for BLE to initialize
static K_SEM_DEFINE(bt_init_ok, 1, 1);

/* Advertising data */
static const struct bt_data ad[] = {
    BT_DATA_BYTES(BT_DATA_FLAGS, (BT_LE_AD_GENERAL | BT_LE_AD_NO_BREDR)),
    BT_DATA(BT_DATA_NAME_COMPLETE, DEVICE_NAME, DEVICE_NAME_LEN)
};

/* Scan response data */
static const struct bt_data sd[] = {
    BT_DATA_BYTES(BT_DATA_UUID128_ALL, BT_UUID_REMOTE_SERV_VAL), 
};

// honeywell_mpr is a default device in the Zephyr ecosystem
const struct device *const pressure_in = DEVICE_DT_GET_ONE(honeywell_mpr); 
static int32_t atm_pressure_kPa;

//Create ADC Buffer
int16_t buf;
struct adc_sequence sequence = {
    .buffer = &buf,
    .buffer_size = sizeof(buf), // bytes
};
// Set initial values
enum out_percentages {brightness_100, brightness_500, pressure};
int16_t out_percentage[3] = {{0}, {0}, {0}};

int64_t adc_readings_100[MAX_ADC_READINGS_100];
int adc_index_100 = 0;

int64_t adc_readings_500[MAX_ADC_READINGS_500];
int adc_index_500 = 0;

int64_t min_array_100[FREQ_100_HZ] = {0};
int64_t max_array_100[FREQ_100_HZ] = {0};

int64_t min_array_500[FREQ_500_HZ] = {0};
int64_t max_array_500[FREQ_500_HZ] = {0};

// Struct for Button Variables
static const struct gpio_dt_spec save_button = GPIO_DT_SPEC_GET(SAVE_BUTTON, gpios);
static const struct gpio_dt_spec send_button = GPIO_DT_SPEC_GET(SEND_BUTTON, gpios);
// Struct for LED Variables
static const struct gpio_dt_spec heartbeat_led = GPIO_DT_SPEC_GET(HEARTBEAT_LED, gpios);
static const struct gpio_dt_spec error_led = GPIO_DT_SPEC_GET(ERROR_LED, gpios);
/* Intialize the ADC struct to store all the DT parameters */
static const struct adc_dt_spec adc_100 = ADC_DT_SPEC_GET_BY_ALIAS(adc100hz);
static const struct adc_dt_spec adc_500 = ADC_DT_SPEC_GET_BY_ALIAS(adc500hz);
static const struct adc_dt_spec adc_battery = ADC_DT_SPEC_GET_BY_ALIAS(batteryadc);
/* Data of ADC io-channels specified in devicetree. */
static const struct adc_dt_spec adc_channels[] = {
	DT_FOREACH_PROP_ELEM(DT_PATH(zephyr_user), io_channels,
			     DT_SPEC_AND_COMMA)
};
// define structs based on DT aliases
static const struct pwm_dt_spec pwm_100Hz = PWM_DT_SPEC_GET(DT_ALIAS(pwm100));
static const struct pwm_dt_spec pwm_500Hz = PWM_DT_SPEC_GET(DT_ALIAS(pwm500));

/* DECLARATIONS */

// Declare State Machine
static const struct smf_state device_states[];
enum device_states {init, run, read, send, vbus, error};
int device_state = init; // initialize device state as init
struct s_object {
        /* This must be first */
        struct smf_ctx ctx;
        /* Events */
        struct k_event smf_event;
        int32_t events;
} s_obj;

// Declare Callback Function
void save_button_callback(const struct device *dev, struct gpio_callback *cb, uint32_t pins);
void send_button_callback(const struct device *dev, struct gpio_callback *cb, uint32_t pins);
void vbus_callback(const struct device *dev, struct gpio_callback *cb, uint32_t pins);
static ssize_t read_data_cb(struct bt_conn *conn, const struct bt_gatt_attr *attr, void *buf, uint16_t len, uint16_t offset);
void data_ccc_cfg_changed_cb(const struct bt_gatt_attr *attr, uint16_t value);
static struct gpio_callback save_button_cb;
static struct gpio_callback send_button_cb;

// Declare Timers, Events, Work Queues
void heartbeat_timer_handler(struct k_timer *heartbeat_timer);
void vbus_timer_handler(struct k_timer *vbus_timer);
void check_vbus_timer_handler(struct k_timer *check_vbus_timer);
void sample_100_led_timer_handler(struct k_timer *sample_100_led_timer);
void sample_500_led_timer_handler(struct k_timer *sample_500_led_timer);
void analyze_sinusoid_handler(struct k_work *analyze_sinusoid);
void read_100_handler(struct k_work *read_100);
void read_500_handler(struct k_work *read_500);

// Declare Bluetooth Functions
void bluetooth_set_battery_level(int level, int nominal_batt_mv);
uint8_t bluetooth_get_battery_level(void);
static struct bt_conn *current_conn;
void on_connected(struct bt_conn *conn, uint8_t ret);
void on_disconnected(struct bt_conn *conn, uint8_t reason);
void on_notif_changed(enum bt_data_notifications_enabled status);
void on_data_rx(struct bt_conn *conn, const uint8_t *const data, uint16_t len);

// Declare Regular Functions
int main(void);
void get_measurements(void);
int64_t read_adc(struct adc_dt_spec adc);
int save_to_array(int64_t value, int64_t *array, int *index, int max_array_length);
void log_array(const int64_t *array, int index, const char *label);
int64_t find_vpp(const int64_t *adc_readings, int adc_index, int points_per_set, int64_t min_array, int64_t max_array);
int64_t calculate_mean(int64_t *array, int size);
int16_t vpp_to_brightness(int64_t vpp, int64_t lower, int64_t upper, struct pwm_dt_spec pwm);
void set_pwm(struct pwm_dt_spec pwm, int num);
void read_pressure(void);
void adjust_endianness(uint8_t* array, size_t size);

/* Setup BLE Services */
BT_GATT_SERVICE_DEFINE(remote_srv,
BT_GATT_PRIMARY_SERVICE(BT_UUID_REMOTE_SERVICE),
BT_GATT_CHARACTERISTIC(BT_UUID_REMOTE_DATA_CHRC,
                BT_GATT_CHRC_READ | BT_GATT_CHRC_NOTIFY,
                BT_GATT_PERM_READ,
                read_data_cb, NULL, NULL),
BT_GATT_CCC(data_ccc_cfg_changed_cb, BT_GATT_PERM_READ | BT_GATT_PERM_WRITE),
);

/* FUNCTION DEFINITIONS */
// Define Timers, Work Queues, Events
K_TIMER_DEFINE(heartbeat_timer, heartbeat_timer_handler, NULL);
K_TIMER_DEFINE(check_vbus_timer, check_vbus_timer_handler, NULL);
K_TIMER_DEFINE(vbus_timer, vbus_timer_handler, NULL);
K_TIMER_DEFINE(sample_100_led_timer, sample_100_led_timer_handler, NULL);
K_TIMER_DEFINE(sample_500_led_timer, sample_500_led_timer_handler, NULL);
K_WORK_DEFINE(analyze_sinusoid, analyze_sinusoid_handler);
K_WORK_DEFINE(read_100, read_100_handler);
K_WORK_DEFINE(read_500, read_500_handler);

// Define Callback Functions
void save_button_callback(const struct device *dev, struct gpio_callback *cb, uint32_t pins) {
    k_event_post(&s_obj.smf_event, SAVE_BTN_PRESS);
}
void send_button_callback(const struct device *dev, struct gpio_callback *cb, uint32_t pins) {
    k_event_post(&s_obj.smf_event, SEND_BTN_PRESS);
}
void vbus_callback(const struct device *dev, struct gpio_callback *cb, uint32_t pins) {
    smf_set_state(SMF_CTX(&s_obj), &device_states[vbus]);
}

// Define Bluetooth Callback Functions
void data_ccc_cfg_changed_cb(const struct bt_gatt_attr *attr, uint16_t value) {
    bool notif_enabled = (value == BT_GATT_CCC_NOTIFY);
    LOG_INF("Notifications: %s", notif_enabled? "enabled":"disabled");

    notifications_enabled = notif_enabled? BT_DATA_NOTIFICATIONS_ENABLED:BT_DATA_NOTIFICATIONS_DISABLED;

    if (remote_service_callbacks.notif_changed) {
        remote_service_callbacks.notif_changed(notifications_enabled);
    }
}

static ssize_t read_data_cb(struct bt_conn *conn, const struct bt_gatt_attr *attr, void *buf, uint16_t len, uint16_t offset) {
    return bt_gatt_attr_read(conn, attr, buf, len, offset, &out_percentage, sizeof(out_percentage));
}

void on_sent(struct bt_conn *conn, void *user_data) {
    ARG_UNUSED(user_data);
    LOG_INF("Notification sent on connection %p", (void *)conn);
}
void bt_ready(int ret) {
    if (ret) {
        LOG_ERR("bt_enable returned %d", ret);
    }

    // release the thread once initialized
    k_sem_give(&bt_init_ok);
}
int send_data_notification(struct bt_conn *conn, uint8_t *value, uint16_t length) {
    int ret = 0;

    struct bt_gatt_notify_params params = {0};
    const struct bt_gatt_attr *attr = &remote_srv.attrs[2];

    params.attr = attr;
    params.data = &value;
    params.len = length;
    params.func = on_sent;

    ret = bt_gatt_notify_cb(conn, &params);

    return ret;
}

/* Initialize the BLE Connection */
int bluetooth_init(struct bt_conn_cb *bt_cb, struct bt_remote_srv_cb *remote_cb) {
    LOG_INF("Initializing Bluetooth");

    if ((bt_cb == NULL) | (remote_cb == NULL)) {
        return -NRFX_ERROR_NULL;
    }
    bt_conn_cb_register(bt_cb);
    remote_service_callbacks.notif_changed = remote_cb->notif_changed;
    remote_service_callbacks.data_rx = remote_cb->data_rx;

    int ret = bt_enable(bt_ready);
    if (ret) {
        LOG_ERR("bt_enable returned %d", ret);
        return ret;
    }

    // hold the thread until Bluetooth is initialized
	k_sem_take(&bt_init_ok, K_FOREVER);

	if (IS_ENABLED(CONFIG_BT_SETTINGS)) {
		settings_load();
	}

	ret = bt_le_adv_start(BT_LE_ADV_CONN, ad, ARRAY_SIZE(ad), sd, ARRAY_SIZE(sd));
	if (ret) {
		LOG_ERR("Could not start advertising (ret = %d)", ret);
		return ret;
	}

	return ret;

}

/* Setup Callbacks */
struct bt_conn_cb bluetooth_callbacks = {
    .connected = on_connected,
    .disconnected = on_disconnected,
};
struct bt_remote_srv_cb remote_service_callbacks = {
    .notif_changed = on_notif_changed,
    .data_rx = on_data_rx,
};
void on_data_rx(struct bt_conn *conn, const uint8_t *const data, uint16_t len) {
    uint8_t temp_str[len+1];
    memcpy(temp_str, data, len);
    temp_str[len] = 0x00; // manually append NULL character at the end
    LOG_INF("BT received data on conn %p. Len: %d", (void *)conn, len);
    LOG_INF("Data: %s", temp_str);
}
void on_connected(struct bt_conn *conn, uint8_t ret) {
    if (ret) { LOG_ERR("Connection error: %d", ret); }
    LOG_INF("BT connected");
    current_conn = bt_conn_ref(conn);
}
void on_disconnected(struct bt_conn *conn, uint8_t reason) {
    LOG_INF("BT disconnected (reason: %d)", reason);
    if (current_conn) {
        bt_conn_unref(current_conn);
        current_conn = NULL;
    }
}
void on_notif_changed(enum bt_data_notifications_enabled status) {
    if (status == BT_DATA_NOTIFICATIONS_ENABLED) {
        LOG_INF("BT notifications enabled");
    } else {
        LOG_INF("BT notifications disabled");
    }
}

// Define States
/* Init State */
static int init_entry(void *o) {
    /* Check that the ADC interface is ready */
    if (!device_is_ready(adc_100.dev)) {
        LOG_ERR("ADC 100 Hz controller device(s) not ready");
        return -1;
    }
    if (!device_is_ready(adc_500.dev)) {
        LOG_ERR("ADC 500 Hz controller device(s) not ready");
        return -1;
    }
    if (!device_is_ready(adc_battery.dev)) {
        LOG_ERR("ADC Battery controller device(s) not ready");
        return -1;
    }
    // LEDS INTERFACE CHECK
    if (!device_is_ready(heartbeat_led.port)) {
        LOG_ERR("gpio 0 interface not ready.");
        return -1;
    }
    // check that the PWM controller is ready
    if (!device_is_ready(pwm_100Hz.dev))  {
        LOG_ERR("PWM device %s is not ready.", pwm_100Hz.dev->name);
        return -1;
    }
    if (!device_is_ready(pwm_500Hz.dev))  {
        LOG_ERR("PWM device %s is not ready.", pwm_500Hz.dev->name);
        return -1;
    }
    // Check MPR Sensor is working
    if (!device_is_ready(pressure_in)) {
        LOG_ERR("MPR pressure sensor %s is not ready", pressure_in->name);
        smf_set_state(SMF_CTX(&s_obj), &device_states[error]);
        return;
	} else {
        // LOG_INF("MPR pressure sensor %s is ready", pressure_in->name);
    }

    // CONFIGURE GPIO BUTTON PINS
    int save_button_pin;
    save_button_pin = gpio_pin_configure_dt(&save_button, GPIO_INPUT);
    if (save_button_pin < 0) {
        LOG_ERR("Cannot configure save button pin.");
        return save_button_pin;
    }
    int send_button_pin;
    send_button_pin = gpio_pin_configure_dt(&send_button, GPIO_INPUT);
    if (send_button_pin < 0) {
        LOG_ERR("Cannot configure send button pin.");
        return send_button_pin;
    }
    // CONFIGURE GPIO LED PINS
    // Configure heartbeat led pin
    int hb_err;
    hb_err = gpio_pin_configure_dt(&heartbeat_led, GPIO_OUTPUT_INACTIVE);
    if (hb_err < 0) {
        LOG_ERR("Cannot configure heartbeat led pin.");
        return hb_err;
    }
    // Configure error led pin
    int error_err;
    error_err = gpio_pin_configure_dt(&error_led, GPIO_OUTPUT_INACTIVE);
    if (error_err < 0) {
        LOG_ERR("Cannot configure error led pin.");
        return error_err;
    }

    /* Configure the ADC channel */
    int adc_100_err;
    adc_100_err = adc_channel_setup_dt(&adc_100);
    if (adc_100_err < 0) {
        LOG_ERR("Could not setup ADC 100 Hz channel (%d)", adc_100_err);
        return adc_100_err;
    }
    int adc_500_err;
    adc_500_err = adc_channel_setup_dt(&adc_500);
    if (adc_500_err < 0) {
        LOG_ERR("Could not setup ADC 500 Hz channel (%d)", adc_500_err);
        return adc_500_err;
    }
    int adc_battery_err;
    adc_battery_err = adc_channel_setup_dt(&adc_battery);
    if (adc_battery_err < 0) {
        LOG_ERR("Could not setup ADC Batteru channel (%d)", adc_battery_err);
        return adc_battery_err;
    }
    // Associate Callback with GPIO Pin
    int save_button_err = gpio_pin_interrupt_configure_dt(&save_button, GPIO_INT_EDGE_TO_ACTIVE);
    if (save_button_err < 0) {
        LOG_ERR("Cannot attach callback to save button");
        return save_button_err;
    }
    gpio_init_callback(&save_button_cb, save_button_callback, BIT(save_button.pin));
    gpio_add_callback(save_button.port, &save_button_cb);
    // Associate Callback with GPIO Pin
    int send_button_err = gpio_pin_interrupt_configure_dt(&send_button, GPIO_INT_EDGE_TO_ACTIVE);
    if (send_button_err < 0) {
        LOG_ERR("Cannot attach callback to send button");
        return send_button_err;
    }
    gpio_init_callback(&send_button_cb, send_button_callback, BIT(send_button.pin));
    gpio_add_callback(send_button.port, &send_button_cb);

    /* Initialize Bluetooth */
    int bt_err = bluetooth_init(&bluetooth_callbacks, &remote_service_callbacks);
    if (bt_err) {
        LOG_ERR("BT init failed (err = %d)", bt_err);
    }
    int pressure_err = read_pressure_sensor(pressure_in, OVERSAMPLE, 0);
    if (pressure_err == -9999) {
        smf_set_state(SMF_CTX(&s_obj), &device_states[error]);
    }
    return 0;
}
static void init_run(void *o) {
    smf_set_state(SMF_CTX(&s_obj), &device_states[run]);
}
static void init_exit(void *o) {
    // set the PWM duty cycle (pulse length)
    set_pwm(pwm_100Hz, 0);
    set_pwm(pwm_500Hz, 0);
    k_timer_start(&heartbeat_timer, K_MSEC(LED_ON_TIME_MS), K_MSEC(LED_ON_TIME_MS));
    k_timer_start(&check_vbus_timer, K_MSEC(LED_ON_TIME_MS), K_MSEC(LED_ON_TIME_MS));
}
/* Run State */
static void run_entry(void *o) {
}
static void run_run(void *o) {
    s_obj.events = k_event_wait(&s_obj.smf_event,
                    SAVE_BTN_PRESS | SEND_BTN_PRESS, true, K_FOREVER);
    struct s_object *s = (struct s_object *)o;
    if (s->events & SAVE_BTN_PRESS) {
        smf_set_state(SMF_CTX(&s_obj), &device_states[read]);
    }
    if (s->events & SEND_BTN_PRESS) {
        smf_set_state(SMF_CTX(&s_obj), &device_states[send]);
    }
}
static void run_exit(void *o) {
}
/* Read State */
static void read_entry(void *o) {
    check_vbus_timer_handler(&check_vbus_timer);
    LOG_INF("Reading ADC");
    get_measurements();
}
static void read_run(void *o) {
    s_obj.events = k_event_wait_all(&s_obj.smf_event,
        (ADC_100_FULL, ADC_500_FULL), true, K_FOREVER);
    struct s_object *s = (struct s_object *)o;
    if (s->events & (ADC_100_FULL | ADC_500_FULL)) {
        // LOG_DBG("Detected");
        k_timer_stop(&sample_100_led_timer);
        k_timer_stop(&sample_500_led_timer);
        k_work_submit(&analyze_sinusoid); // Submit analysis work for arrays
        smf_set_state(SMF_CTX(&s_obj), &device_states[run]);
    }
}
static void read_exit(void *o) {
}
/* Send State */
static void send_entry(void *o) {
    set_pwm(pwm_100Hz, 0);
    set_pwm(pwm_500Hz, 0);
    uint64_t bat_val_mv = read_adc(adc_battery);
    LOG_INF("Battery ADC Value (mV): %lld", bat_val_mv);
    int16_t battery_level = 0;
    battery_level = ((bat_val_mv)/MAX_BATTERY_DV);
    LOG_INF("Battery Percentage: %d", battery_level);
    /* Example of how to use the Battery Service
    `normalized_level` comes from an ADC reading of the battery voltage
    this function populates the BAS GATT with information
    */
    int bat_err = bt_bas_set_battery_level((int)battery_level);
    if (bat_err) {
        LOG_ERR("BAS set error (bat_err = %d)", bat_err);
    }

    // this function retrieves BAS GATT with information
    battery_level =  bt_bas_get_battery_level();
}
static void send_run(void *o) {

    LOG_DBG("Exported 100 Hz LED Brightness: %d", out_percentage[brightness_100]);
    LOG_DBG("Exported 500 Hz LED Brightness: %d", out_percentage[brightness_500]);
    LOG_DBG("Exported Atmospheric Pressure (Pa): %d", out_percentage[pressure]);

    adjust_endianness(out_percentage, sizeof(out_percentage));
    int bt_err;
    bt_err = send_data_notification(current_conn, out_percentage, 3);
    if (bt_err) {
        LOG_ERR("Could not send BT notification (err: %d)", bt_err);
    }
    else {
        LOG_INF("BT data transmitted.");
    }
    smf_set_state(SMF_CTX(&s_obj), &device_states[run]);
}
static void send_exit(void *o) {
}
/* VBUS State */
static void vbus_entry(void *o) {
    k_timer_stop(&check_vbus_timer);
    k_timer_start(&vbus_timer, K_MSEC(LED_ON_TIME_MS), K_MSEC(LED_ON_TIME_MS));
    LOG_INF("Hey idiot you plugged something into VBUS again");
}
static void vbus_run(void *o) {
    k_msleep(5000);
    smf_set_state(SMF_CTX(&s_obj), &device_states[run]);
}
static void vbus_exit(void *o) {
    LOG_DBG("See ya VBUS");
    k_timer_stop(&vbus_timer);
    k_timer_start(&check_vbus_timer, K_MSEC(LED_ON_TIME_MS), K_MSEC(LED_ON_TIME_MS));
    gpio_pin_set_dt(&error_led, 0);
}
/* Error State */
static void error_entry(void *o) {
    LOG_INF("Hey! You forgot to attach your sensor!");
    gpio_pin_set_dt(&error_led, 1);
}
static void error_run(void *o) {
        atm_pressure_kPa = read_pressure_sensor(pressure_in, OVERSAMPLE, 0);
        if (atm_pressure_kPa != -9999) {
            smf_set_state(SMF_CTX(&s_obj), &device_states[run]);
        }
        k_msleep(3000);
}
static void error_exit(void *o) {
    LOG_DBG("Good to go with sensor");
    gpio_pin_set_dt(&error_led, 0);
}
/* Populate state table */
static const struct smf_state device_states[] = {
        /* Init State */
        [init] = SMF_CREATE_STATE(init_entry, init_run, init_exit),
        /* Run State */
        [run] = SMF_CREATE_STATE(run_entry, run_run, run_exit),
        /* Read State */
        [read] = SMF_CREATE_STATE(read_entry, read_run, read_exit),
        /* Send State */
        [send] = SMF_CREATE_STATE(send_entry, send_run, send_exit),
        /* VBUS State */
        [vbus] = SMF_CREATE_STATE(vbus_entry, vbus_run, vbus_exit),
        /* Error State */
        [error] = SMF_CREATE_STATE(error_entry, error_run, error_exit),
};
// Define Main
int main(void) {  
    int32_t ret = 0;
    k_event_init(&s_obj.smf_event);
    // Set initial state as init
    smf_set_initial(SMF_CTX(&s_obj), &device_states[init]);
    while (1) {
        ret = smf_run_state(SMF_CTX(&s_obj));
        if (ret) {
            /* handle return code and terminate state machine */
            break;
        }
        k_msleep(SLEEP_TIME_MS);
    }
    return ret;
}
/* Define Functions */
// Beging Recording ADC values
void get_measurements(void) {
    adc_index_100 = 0;
    adc_index_500 = 0;
    read_pressure();
    k_timer_start(&sample_100_led_timer, K_USEC(RECORD_TIME_100_US), K_USEC(RECORD_TIME_100_US));
    k_timer_start(&sample_500_led_timer, K_USEC(RECORD_TIME_500_US), K_USEC(RECORD_TIME_500_US));
}
// Read in ADC function
int64_t read_adc(struct adc_dt_spec adc) {
    adc_sequence_init_dt(&adc, &sequence);
    int ret;
    ret = adc_read(adc_channels[adc.channel_id].dev, &sequence);
    if (ret < 0) {
        LOG_ERR("Could not read (%d)", ret);
    } else {
    //    LOG_DBG("Raw 100 Hz ADC Buffer: %d", buf);
    }
    uint64_t val_mv;
    val_mv = buf;
    ret = adc_raw_to_millivolts_dt(&adc, &val_mv); // remember that the vadc struct contains all the DT parameters
    if (ret < 0) {
        LOG_ERR("Buffer cannot be converted to mV; returning raw buffer value.");
    } else {
        // LOG_INF("ADC Value (mV): %lld", val_mv);
    }
    return val_mv;
}
// Save value to an array
int save_to_array(int64_t value, int64_t *array, int *index, int max_array_length) {
    if (*index < max_array_length) {
        array[(*index)++] = value;
        return 0;
    } else {
        return -1;
    }
}
// LOG an Array's Values
void log_array(const int64_t *array, int index, const char *label) {
    LOG_INF("%s:", label);
    for (int i = 0; i < index; ++i) {
        LOG_INF("%lld", array[i]);
    }
}
// Find Vpp from array
int64_t find_vpp(const int64_t *adc_readings, int adc_index, int points_per_set, int64_t min_array, int64_t max_array) {
    int num_sets = adc_index / points_per_set;
    int min_index = 0;
    int max_index = 0;
    for (int set = 0; set < num_sets; ++set) {
        int start_index = set * points_per_set;
        
        int64_t min_value = adc_readings[start_index];
        int64_t max_value = adc_readings[start_index];

        for (int i = start_index; i < (start_index + points_per_set); ++i) {
            if (adc_readings[i] < min_value) {
                min_value = adc_readings[i];
            }
            if (adc_readings[i] > max_value) {
                max_value = adc_readings[i];
            }
        }
        // Log the minimum and maximum values for this set
        // LOG_INF("Set %zu - Min Value: %lld, Max Value: %lld", set + 1, min_value, max_value);
        save_to_array(min_value, min_array, &min_index, num_sets);
        save_to_array(max_value, max_array, &max_index, num_sets);
    }
    // log_array(min_array, min_index, "Minimum Values:");
    // log_array(max_array, max_index, "Maximum Values:");
    int64_t avg_min = calculate_mean(min_array, min_index);
    int64_t avg_max = calculate_mean(max_array, max_index);
    // LOG_DBG("Average Minimum Voltage: %lld", avg_min);
    // LOG_DBG("Average Maximum Voltage: %lld", avg_max);
    int64_t Vpp = avg_max - avg_min;
    LOG_DBG("Vpp: %lld", Vpp);
    return Vpp;
}
// Average an array
int64_t calculate_mean(int64_t *array, int size) {
    int64_t sum = 0;
    for (int i = 0; i < size; i++) {
        sum += array[i];
    }
    int64_t average = sum / size;
    return average;
}
// Convert Vpp to Brightness
int16_t vpp_to_brightness(int64_t vpp, int64_t lower, int64_t upper, struct pwm_dt_spec pwm) {
  
    int64_t brightness = ((MAX_LED_BRIGHTNESS*(vpp - lower)))/(upper-lower);
    int64_t brightness_percentage = brightness/PWM_BRIGHTNESS_TO_PERCENT;
    LOG_DBG("Brightness: %lld", brightness);
    LOG_DBG("Brightness Percentage: %lld", brightness_percentage);
    set_pwm(pwm, brightness);
    return (int16_t)brightness_percentage;
}
// Set PWM
void set_pwm(struct pwm_dt_spec pwm, int num) {
    int err;
    err = pwm_set_pulse_dt(&pwm, num);
    if (err) {
        LOG_ERR("Could not set pwm guy (PWM0)");
    }
}
// Measure Pressure
void read_pressure(void) { 
        atm_pressure_kPa = read_pressure_sensor(pressure_in, OVERSAMPLE, 0);
        if (atm_pressure_kPa == -9999) {
            smf_set_state(SMF_CTX(&s_obj), &device_states[error]);
        } else {
            LOG_DBG("Pressure: %d", atm_pressure_kPa);
            out_percentage[pressure] = atm_pressure_kPa;
        }
        k_msleep(MEASUREMENT_DELAY_MS);
}
// Swap Endian-ness
void adjust_endianness(uint8_t* array, size_t size) {
    for (size_t i = 0; i < size; i += 2) {
        uint8_t temp = array[i];
        array[i] = array[i + 1];
        array[i + 1] = temp;
    }
}
// Define Timer Functions
void heartbeat_timer_handler(struct k_timer *heartbeat_timer) {
    gpio_pin_toggle_dt(&heartbeat_led);
}
void vbus_timer_handler(struct k_timer *vbus_timer) {
    gpio_pin_toggle_dt(&error_led);
    uint32_t usbregstatus = nrf_power_usbregstatus_vbusdet_get(NRF_POWER);
    if (!(usbregstatus)) {
        LOG_INF("Vbus disconnected");
        smf_set_state(SMF_CTX(&s_obj), &device_states[run]);
    }
}
void check_vbus_timer_handler(struct k_timer *check_vbus_timer) {
    uint32_t usbregstatus = nrf_power_usbregstatus_vbusdet_get(NRF_POWER);
    if (usbregstatus) {
        LOG_INF("Vbus detected");
        smf_set_state(SMF_CTX(&s_obj), &device_states[vbus]);

    } else {
        // LOG_DBG("Oh man I didn't find it");
    }
}
void sample_100_led_timer_handler(struct k_timer *sample_100_led_timer) {
    k_work_submit(&read_100);
}
void sample_500_led_timer_handler(struct k_timer *sample_500_led_timer) {
    k_work_submit(&read_500);
}
// Define Work Queues
void analyze_sinusoid_handler(struct k_work *analyze_sinusoid) {
    int64_t lower_100 = 5;
    int64_t upper_100 = 50;
    int64_t lower_500 = 10;
    int64_t upper_500 = 150;
    int64_t Vpp_100Hz = find_vpp(adc_readings_100, adc_index_100, POINTS_PER_SET_100_HZ, min_array_100, max_array_100);
    int64_t Vpp_500Hz = find_vpp(adc_readings_500, adc_index_500, POINTS_PER_SET_500_HZ, min_array_500, max_array_500);
    int16_t brightness_100Hz = vpp_to_brightness(Vpp_100Hz, lower_100, upper_100, pwm_100Hz);
    int16_t brightness_500Hz = vpp_to_brightness(Vpp_500Hz, lower_500, upper_500, pwm_500Hz);
    out_percentage[brightness_100] = brightness_100Hz;
    out_percentage[brightness_500] = brightness_500Hz;
}
void read_100_handler(struct k_work *read_100) {
    uint64_t val_mv_100 = read_adc(adc_100);
    // LOG_DBG("100 Hz ADC: %d", val_mv_100);
    int ret = save_to_array(val_mv_100, adc_readings_100, &adc_index_100, MAX_ADC_READINGS_100);
    if (ret < 0) {
        // LOG_DBG("100 Full");
        k_event_post(&s_obj.smf_event, ADC_100_FULL);
    }
}
void read_500_handler(struct k_work *read_500) {
    uint64_t val_mv_500 = read_adc(adc_500);
    // LOG_DBG("500 Hz ADC: %d", val_mv_500);
    int ret = save_to_array(val_mv_500, adc_readings_500, &adc_index_500, MAX_ADC_READINGS_500);
    if (ret < 0) {
        // LOG_DBG("500 Full");
        k_event_post(&s_obj.smf_event, ADC_500_FULL);
    }
}
