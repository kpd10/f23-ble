[![pipeline status](https://gitlab.oit.duke.edu/EmbeddedMedicalDevices/f23-ble/badges/main/pipeline.svg)](https://gitlab.oit.duke.edu/EmbeddedMedicalDevices/f23-ble/-/commits/main)

# BME590L F23-BLE Lab

Consult the [Zephyr-BLE](https://gitlab.oit.duke.edu/EmbeddedMedicalDevices/zephyr-ble/-/blob/main/lab/Zephyr-BLE-Lab.md?ref_type=heads) lab for all details.  Some reminders:
* You should `Fork` this repository (not `clone` it!).
* Add Dr. Palmeri (`mlp6`) as a Maintainer.
* If you choose to work with a partner, add them as a Maintainer too.
* Each person should work on their own branches and merge them into `main` when "done".
* Submit a `Merge Request` to this repository when ready to submit everything.
  + The title of the merge request should be: "Draft: FullName1 [FullName2]"
  + Assign the merge request to Dr. Palmeri (`mlp6`).


Issues to deal with:
- Not recording all the values I want in 1 second, leaving off the last one because it stops firing the duration timer before the ADC value can be read in
- Vpp is getting doubled when I read in ADC
- Make output sinusoidal PWV (calculate normalized sin values outside and bring in as an array)